[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)

## ROBOT
### Anith sir gave a task to all the grade 11 students to create a walking robot using specific materials like motors, batteries, cardboard boxes, rubber bands, glue sticks, battery holders, popsicle sticks, wires, long toothpicks, and potentiometers (optional).
- Here is the rough sketch or design we have created for our robot. The four of us discussed and developed this idea together.
![alt text](robot_image1.jpg)
![alt text](robot_image2.jpg)
- Here are the materials that we utilized in the construction of our robot, all of which were provided to us by our teacher for this project.
![alt text](robot_image4.jpg)
![alt text](robot_image5.jpg)
- This is the 3D-designed sketch of our tortoise displayed on the whiteboard, with the entire design created by Kinley Wangmo.
![alt text](robot_image3.jpg)
- Sonam Pelmo dedicated almost thirty minutes to carefully designing the legs and arms of our tortoise using TinkerCad, contributing significantly to the overall project.
![alt text](robot_image6.jpg)
![alt text](robot_image7.jpg)
![alt text](robot_image8.jpg)

- The tortoise, designed collaboratively by Deki Seldon and Sonam Tshomo, required nearly twenty minutes to complete, providing us with valuable insights during the construction process.
![alt text](tortoise1.png)

- Deki Seldon and Sonam Pelmo collaborated to design a circular gear and other parts of the robot using Tinkercad, a process that took approximately 20-25 minutes, highlighting the efficiency of teamwork in our project.
![alt text](gear_image.png)
![alt text](part_robot1.png)
![alt text](part_robot2.png)

- The base of the tortoise is designed using a combination of a trapezoid and hexagon, ensuring a stable body that aligns with our project's name and reflects our aesthetic vision.
![alt text](hexagon_image.png)
![alt text](trapezoid1.png)

- Sonam Wangchuk assisted in developing the laser cutting process.
![alt text](laser_cutter1.png)
![alt text](laser_cutter2.jpg)

- Kinley Wangmo dedicated approximately 15 minutes of her valuable time overseeing the laser cutting process.
![alt text](laser_cutter3.jpg)
![alt text](laser_cutter4.jpg)
- Sonam Wangchuk is guiding the Hibicus team through the process. 
![alt text](laser_process2.jpg)
![alt text](laser_process3.jpg)
- The product after completing the laser cutting process.
![alt text](cartbox1.jpg)
![alt text](cartbox2.jpg)
- Trial and error in the development of our robot.
![alt text](ROBOT1.jpg)
![alt text](ROBOT2.jpg)
![alt text](ROBOT3.jpg)
![alt text](ROBOT4.jpg)
![alt text](ROBOT5.jpg)
- This is our failed project and now, we are working on our new project. 
![alt text](ROBOT6.jpg)

- This is our third trial. The first trial had a lot of flaws like imbalance and inequal distances between the gears. In the second trial, we couldn't get accurate/needed diameter for the circles and also all the circles for the gear were in different sizes which led to collision of the circles and also disturbtion of rubber bands. In this third trial, we could finally come up with the walking bot that we expected.
 ![alt text](final2.jpg)

 - We are using A4 sized paper to make a pair of wings for our grasshoper. We chose to use it because it is light and it is convenient to paint the wings on it. 
 ![alt text](wings_robot.jpg)
 - We would like to mention that our initial plan was to make a tortoise but as we go through the process, we had to change our plan to a grasshoper because when we tested our walking bot, it jumped like a grasshoper. 
 
[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)