[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)

# PROJECT HOMEPAGE
![Alt text](<humidifier.jpg>)
#### Our group is going to create a humidifier along with a lamp. Project insights will be shown below. 

## Project Concept: 
### Applications of the Project:
 We could use the lamp-humidifier in the classrooms in the Dzong during our learning experiences. We could also use this in our rooms in the dorms. This lamp-humidifier has two functions, so it can be used to its fullest potential in the dorms when we have to put off the lights. Using a humidifier can prevent the students from getting cough and cold during dry seasons. As this project integrates two things(the lamp and the humidifier) into one, it can be efficient in terms of utilizing the space. 

## Inspirations: 
Our group first got the idea when a teacher mentioned about our school's rooms having very dry conditions and also mentioned about how these rooms needed humidity. Then an idea struck us which is to make a humidifier of our own. We then thought of modifying the humidifier and adding new features to it. 

## Expected Outcomes:
We hope that our project will be successful and come out to be beautiful and productive. We hope as a team that our project will be a sustainable one and can be used for a long duration. We hope that our project can benefit as many people as possible. 

[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)