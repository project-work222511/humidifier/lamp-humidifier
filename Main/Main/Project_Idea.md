[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)

## LAMP-HUMIDIFIER

The lack of humidity in the classes are the issues that we think that can make everyone get cough and cold. So we thought that placing something to create humidity would be a great idea. The humidifier has a humidity sensor as well as a temperature sensor and when the humidity as well as the temperature is low, it will automatically humidify the classes reducing the risk of getting a cough and cold. 

## Research Findings:
We have found out that humidifiers with mist makers can be integrated with Arduino microcontrollers to create smart and automated humidification systems. In such setups, the mist maker is typically connected to the Arduino board, which acts as the control unit. The Arduino board is programmed to monitor environmental conditions such as humidity levels using sensors. When the humidity drops below a certain threshold, the Arduino sends a signal to activate the mist maker, which then generates mist to increase the humidity in the room. Additionally, Arduino can be programmed to regulate the intensity of mist production based on user-defined settings or real-time data from the sensors, providing precise control over the humidification process.

Furthermore, Arduino can be combined with other components such as OLED displays to enhance its features and user interaction. For instance, an OLED display can be used to show real-time humidity levels and system status. 

## Online references:
We have referred to following resources: 
Chen, W. (2018, January 22). My first Arduino Project - Automatic Humidifier [Video]. YouTube. https://youtu.be/oZgfAW2DMz4?si=81pG2hwP0qx20BfI

![Alt text](screenshot-1.png)
![Alt text](screenshot-2.png)

## Compeonents Required for the Project 
### Adafruit METRO 328:
 This board is the same as Arduino UNO but with a few more features than UNO. Since it is both a software and a hardware, we can use it to receive data from the humidity and temperature sensors and send command to the mist maker to start humidifying. So, adafruit METRO 328 can be used to control our circuit. Then, this board requires USB-A which is readily available with us which can be of great help when coding. 

![Alt text](<METRO 328.png>)

### Mist maker:
 This component is the most crucial in our project. It is necessary for the humidifier to have this component as it is the source of mist which humidifies our rooms. It can turn liquid into gas at a fast rate. 

![Alt text](<Ultrasonic Mistmaker.png>)

### OLED:
 This is a component which helps diplay the temperature and the humidity of the room. If possible, we will try to display the on and off state of the mist maker as well as the lamp.

![Alt text](<OLED.png>)
 
### The LED module:
It is suitable for a lamp-humidifier project because it is small, energy-efficient, and doesn't produce much heat. It lasts a long time, so we won't need to replace it often. On top of that we are using the LED modules of broken lamps which proves to be sustainable for our environment. 

![Alt text](<LED module.png>)

### 3-D printed and laser cut components: 
We need this components as it is readily available in our school and it best suits our design as we customize the type of design we want to make our project attractive and good.
### 3D- Printer
![Alt text](<3-D printer.png>)
### Laser cutter
![Alt text](<Laser cutter.png>)

### Water container: 
We have used the container of an old broken humidifier to contain the water required for mist making. 

### Temperature and humidity sensors: 
We need these components to sense the temperature the humidity of a room so that we are aware of the how much humidity we need and to send the information to the METRO board to control our mist maker. These components can make our project to be an automatic one. 
![Alt text](Temp_Humidity_sensor.png)

### Breadboard and jumper wires:
 We need these for connecting our components with METRO and other components.
 
 ![Alt text](<Jumper wire.png>) 
 
[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)