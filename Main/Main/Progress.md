[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)

### Curcuit Design for LED
Currently, our team has been focused on creating a circuit for our lamp. Specifically, we have developed a circuit for the LED component, complete with its own switch. The inspiration for this design came from Deki Seldon, with assistance from Sonam Pelmo and Kinley Wangmo in finalizing the circuit.
- This marked the beginning of the circuit construction process, which will continue until completion.
![Alt text](circuit_2.png)
![Alt text](circuit_1.png)
![Alt text](circuit_3.png)
- This represents our completed construction.
![Alt text](image.png)

### Coding for Lamp Circuit
- Our group has programmed the lamp circuit, with Deki Seldon taking charge of the coding.
![Alt text](coding_1.png)
![Alt text](coding_2.png)

### Box Design
- We were assigned to build a box as our primary project, and Sonam Pelmo designed it using TinkerCad.
![Alt text](<Box_Design.png>)
![Alt text](<box_design.png>)

### Working model of LED stripe
- We required an LED strip, and we discovered one in our campus FabLab.

### Link OLED to humidity and temperature sensor.
- Here are photos of our teammate connecting the wires.
![alt text](Link_image1.jpg)
![alt text](Link_image2.jpg)

- On our first try, the humidity and temperature sensor burned out.
![alt text](Link_image4.jpg)

- We requested an additional humidity and temperature sensor and attempted to use it. Before we burned the humidity and temperature sensor again, we checked the OLED.
![alt text](Link_image3.jpg)

- Verifying if the mist maker is functioning properly.
![alt text](Link_image5.jpg)

- Inspecting the mist maker with the water.
![alt text](mist_try.jpg)

- The mist maker is finally functioning.
![alt text](working_mist.jpg)
<video controls src="working_mist.mp4" title="Title"></video>

- Due to time constraints, we chose to download an existing box design from the internet and use a laser cutter to produce it, rather than designing a new one ourselves.
- This is the design we sourced online for our lamp humidifier.
![alt text](project_outer.png)

- Here is our team assembling the components after completing the laser cutting process.
![alt text](project.jpg)

- Here is our team organizing and placing the components inside the box.
![alt text](project_1.jpg)

- Kinley Wangmo and Sonam Tshomo: Connecting the LED by soldering the wires.
![alt text](project_2.jpg)

- Verifying if the components function properly together.
![alt text](project_3.jpg)

- After connecting the USB to the laptop, the humidity sensor failed, but we stayed hopeful.


[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)