[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)

## Terminologies: 
### OLED:
 OLED stands for Organic Light-Emitting Diode. It's a type of display technology that uses organic compounds to emit light when an electric current is applied. OLED displays offer advantages like vibrant colors, deep blacks, and flexibility in design.
![Alt text](<OLED.png>)
### LED module:

LED module refers to a component containing Light-Emitting Diodes (LEDs). LEDs are semiconductor devices that emit light when an electric current passes through them. LED modules are commonly used for illumination in various applications due to their energy efficiency and long lifespan.
![Alt text](<LED module.png>)

### Adrafruit METRO board:
The Adafruit METRO board is a microcontroller board based on the Arduino platform. It's designed for use in electronics projects and features an ATmega328 microcontroller, USB interface, and various input/output pins. The METRO board is compatible with the Arduino IDE and can be programmed using the Arduino programming language.

![Alt text](<METRO 328.png>)

### Mist maker:
 A mist maker is a device that uses ultrasonic vibrations to create a fine mist or fog by breaking water into tiny droplets. It's commonly used in humidifiers and decorative foggers. Mist makers typically consist of a vibrating element, water reservoir, and a mechanism to disperse the mist into the air.
 ![Alt text](<Ultrasonic Mistmaker.png>)

### Humidity: 
Humidity refers to the amount of water vapor present in the air. It's an essential aspect of the atmospheric environment and can affect factors such as comfort, health, and the performance of certain materials and equipment. Humidity levels are often measured as relative humidity, which expresses the amount of water vapor present relative to the maximum amount the air can hold at a given temperature.
![Alt text](<Humidity.png>)

[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)