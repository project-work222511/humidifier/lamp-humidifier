[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)

# Team Hibicus members

![Alt text](Deki_Seldon.png)
### Deki Seldon
### Age: 17
### Hobbies: I like playing football and spending time with my loved ones. I enjoy cooking too.
### Learning Goals: I want to learn how to connect circuits, 3-D printing, laser cutting and also about the coding. 

![Alt text](Kinley_Wangmo.jpg)
### Kinley Wangmo
### Age: 17
### Hobbies: Writing, reading and explorations.
### Learning Goals: Basic designs, coding and circuit connections.

![Alt text](Sonam_Pelmo.png)
### Sonam Pelmo 
### Age: 17
### Hobbies: Reading, drawing, painting, and spending time with my friends
### Learning Goals: I want to be familiarized with coding for Arduino and METRO boards. I also want to be able to be familair with connecting the components in a circuit. 

![Alt text](Sonam_Tshomo.png)
### Sonam Tshomo
### Age: 16
### Hobbies: Reading, hiking, listening to music, and spending time with animals. 
### Learning Goals: I want to be familiarized with Gitlab and 3-D prnting and also laser cutting. 


## Strategies for peer learning within the team
1. Effective communication
2. Research and explaination to each other 
3. Being risk takers and doing things practically
4. Exploration and learning from online resources and from experts

[Home](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/index.md?ref_type=heads)|[Progress](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Progress.md)|[Project Idea](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Project_Idea.md?ref_type=heads)|[Team](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Team.md?ref_type=heads)|[Terminology](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Terminology.md?ref_type=heads)|[Robot](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/robot.md?ref_type=heads)|[Bibilography](https://gitlab.com/project-work222511/humidifier/lamp-humidifier/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)